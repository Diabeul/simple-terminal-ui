use rustbox::{Event, InitOptions, Key, RustBox};
use std::time::Duration;

use crate::prelude::*;
use crate::text::Text;

enum TuiEvent {
    Drawing,
    Exiting,
}

pub struct Tui {
    rb: RustBox,
    frame: Duration,
}

impl Tui {
    pub fn new(frame_rate: u64) -> Self {
        Self {
            rb: RustBox::init(InitOptions::default()).unwrap(),
            frame: Duration::from_millis(frame_rate),
        }
    }

    pub fn draw_text(&self, txt: &str) -> Result<()> {
        let text: Text = txt.parse()?;
        let start_x = self.rb.width() / 2 - text.total_width() / 2;
        let start_y = self.rb.height() / 2 - 3;

        self.rb.clear();
        text.draw(&self.rb, start_x, start_y);
        self.rb.present();

        Ok(())
    }

    pub fn render(&self, txt: &str) -> Result<()> {
        while !self.is_finished() {
            self.draw_text(txt)?
        }
        Ok(())
    }

    pub fn is_finished(&self) -> bool {
        matches!(self.handle_events(), TuiEvent::Exiting)
    }

    fn handle_events(&self) -> TuiEvent {
        match self.rb.peek_event(self.frame, false).unwrap() {
            Event::KeyEvent(Key::Esc | Key::Ctrl('c')) => TuiEvent::Exiting,
            _ => TuiEvent::Drawing,
        }
    }
}

impl Default for Tui {
    fn default() -> Self {
        Self::new(16)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_should_draw() {
        todo!()
    }
}
