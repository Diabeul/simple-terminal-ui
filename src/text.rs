use std::collections::HashMap;
use std::panic;
use std::str::FromStr;

use once_cell::sync::Lazy;
use rustbox::{Color, RustBox};

use crate::prelude::*;
use thiserror::Error;

type Font<'a> = Lazy<HashMap<char, Letter<'a>>>;

static FONT: Font = Lazy::new(|| {
    HashMap::from([
        (':', Letter::new(["   ", "██╗", "╚═╝", "██╗", "╚═╝", "   "])),
        (
            '-',
            Letter::new(["      ", "      ", "█████╗", "╚════╝", "      ", "      "]),
        ),
        (
            '_',
            Letter::new([
                "       ",
                "       ",
                "       ",
                "       ",
                "██████╗",
                "╚═════╝",
            ]),
        ),
        ('.', Letter::new(["   ", "   ", "   ", "   ", "██╗", "╚═╝"])),
        ('!', Letter::new(["██╗", "██║", "██║", "╚═╝", "██╗", "╚═╝"])),
        (
            '0',
            Letter::new([
                " ██████╗ ",
                "██╔═████╗",
                "██║██╔██║",
                "████╔╝██║",
                "╚██████╔╝",
                " ╚═════╝ ",
            ]),
        ),
        (
            '1',
            Letter::new([" ██╗", "███║", "╚██║", " ██║", " ██║", " ╚═╝"]),
        ),
        (
            '2',
            Letter::new([
                "██████╗ ",
                "╚════██╗",
                " █████╔╝",
                "██╔═══╝ ",
                "███████╗",
                "╚══════╝",
            ]),
        ),
        (
            '3',
            Letter::new([
                "██████╗ ",
                "╚════██╗",
                " █████╔╝",
                " ╚═══██╗",
                "██████╔╝",
                "╚═════╝ ",
            ]),
        ),
        (
            '4',
            Letter::new([
                "██╗  ██╗",
                "██║  ██║",
                "███████║",
                "╚════██║",
                "     ██║",
                "     ╚═╝",
            ]),
        ),
        (
            '5',
            Letter::new([
                "███████╗",
                "██╔════╝",
                "███████╗",
                "╚════██║",
                "███████║",
                "╚══════╝",
            ]),
        ),
        (
            '6',
            Letter::new([
                " ██████╗ ",
                "██╔════╝ ",
                "███████╗ ",
                "██╔═══██╗",
                "╚██████╔╝",
                " ╚═════╝ ",
            ]),
        ),
        (
            '7',
            Letter::new([
                "███████╗",
                "╚════██║",
                "    ██╔╝",
                "   ██╔╝ ",
                "   ██║  ",
                "   ╚═╝  ",
            ]),
        ),
        (
            '8',
            Letter::new([
                " █████╗ ",
                "██╔══██╗",
                "╚█████╔╝",
                "██╔══██╗",
                "╚█████╔╝",
                " ╚════╝ ",
            ]),
        ),
        (
            '9',
            Letter::new([
                " █████╗ ",
                "██╔══██╗",
                "╚██████║",
                " ╚═══██║",
                " █████╔╝",
                " ╚════╝ ",
            ]),
        ),
        (
            'a',
            Letter::new([
                " █████╗ ",
                "██╔══██╗",
                "███████║",
                "██╔══██║",
                "██║  ██║",
                "╚═╝  ╚═╝",
            ]),
        ),
        (
            'b',
            Letter::new([
                "██████╗ ",
                "██╔══██╗",
                "██████╔╝",
                "██╔══██╗",
                "██████╔╝",
                "╚═════╝ ",
            ]),
        ),
        (
            'c',
            Letter::new([
                " ██████╗",
                "██╔════╝",
                "██║     ",
                "██║     ",
                "╚██████╗",
                " ╚═════╝",
            ]),
        ),
        (
            'd',
            Letter::new([
                "██████╗ ",
                "██╔══██╗",
                "██║  ██║",
                "██║  ██║",
                "██████╔╝",
                "╚═════╝ ",
            ]),
        ),
        (
            'e',
            Letter::new([
                "███████╗",
                "██╔════╝",
                "█████╗  ",
                "██╔══╝  ",
                "███████╗",
                "╚══════╝",
            ]),
        ),
        (
            'f',
            Letter::new([
                "███████╗",
                "██╔════╝",
                "█████╗  ",
                "██╔══╝  ",
                "██║     ",
                "╚═╝     ",
            ]),
        ),
        (
            'g',
            Letter::new([
                " ██████╗ ",
                "██╔════╝ ",
                "██║  ███╗",
                "██║   ██║",
                "╚██████╔╝",
                " ╚═════╝ ",
            ]),
        ),
        (
            'h',
            Letter::new([
                "██╗  ██╗",
                "██║  ██║",
                "███████║",
                "██╔══██║",
                "██║  ██║",
                "╚═╝  ╚═╝",
            ]),
        ),
        ('i', Letter::new(["██╗", "██║", "██║", "██║", "██║", "╚═╝"])),
        (
            'j',
            Letter::new([
                "     ██╗",
                "     ██║",
                "     ██║",
                "██   ██║",
                "╚█████╔╝",
                " ╚════╝ ",
            ]),
        ),
        (
            'k',
            Letter::new([
                "██╗  ██╗",
                "██║ ██╔╝",
                "█████╔╝ ",
                "██╔═██╗ ",
                "██║  ██╗",
                "╚═╝  ╚═╝",
            ]),
        ),
        (
            'l',
            Letter::new([
                "██╗     ",
                "██║     ",
                "██║     ",
                "██║     ",
                "███████╗",
                "╚══════╝",
            ]),
        ),
        (
            'm',
            Letter::new([
                "███╗   ███╗",
                "████╗ ████║",
                "██╔████╔██║",
                "██║╚██╔╝██║",
                "██║ ╚═╝ ██║",
                "╚═╝     ╚═╝",
            ]),
        ),
        (
            'n',
            Letter::new([
                "███╗   ██╗",
                "████╗  ██║",
                "██╔██╗ ██║",
                "██║╚██╗██║",
                "██║ ╚████║",
                "╚═╝  ╚═══╝",
            ]),
        ),
        (
            'o',
            Letter::new([
                " ██████╗ ",
                "██╔═══██╗",
                "██║   ██║",
                "██║   ██║",
                "╚██████╔╝",
                " ╚═════╝ ",
            ]),
        ),
        (
            'p',
            Letter::new([
                "██████╗ ",
                "██╔══██╗",
                "██████╔╝",
                "██╔═══╝ ",
                "██║     ",
                "╚═╝     ",
            ]),
        ),
        (
            'q',
            Letter::new([
                " ██████╗ ",
                "██╔═══██╗",
                "██║   ██║",
                "██║▄▄ ██║",
                "╚██████╔╝",
                " ╚══▀▀═╝ ",
            ]),
        ),
        (
            'r',
            Letter::new([
                "██████╗ ",
                "██╔══██╗",
                "██████╔╝",
                "██╔══██╗",
                "██║  ██║",
                "╚═╝  ╚═╝",
            ]),
        ),
        (
            's',
            Letter::new([
                "███████╗",
                "██╔════╝",
                "███████╗",
                "╚════██║",
                "███████║",
                "╚══════╝",
            ]),
        ),
        (
            't',
            Letter::new([
                "████████╗",
                "╚══██╔══╝",
                "   ██║   ",
                "   ██║   ",
                "   ██║   ",
                "   ╚═╝   ",
            ]),
        ),
        (
            'u',
            Letter::new([
                "██╗   ██╗",
                "██║   ██║",
                "██║   ██║",
                "██║   ██║",
                "╚██████╔╝",
                " ╚═════╝ ",
            ]),
        ),
        (
            'v',
            Letter::new([
                "██╗   ██╗",
                "██║   ██║",
                "██║   ██║",
                "╚██╗ ██╔╝",
                " ╚████╔╝ ",
                "  ╚═══╝  ",
            ]),
        ),
        (
            'w',
            Letter::new([
                "██╗    ██╗",
                "██║    ██║",
                "██║ █╗ ██║",
                "██║███╗██║",
                "╚███╔███╔╝",
                " ╚══╝╚══╝ ",
            ]),
        ),
        (
            'x',
            Letter::new([
                "██╗  ██╗",
                "╚██╗██╔╝",
                " ╚███╔╝ ",
                " ██╔██╗ ",
                "██╔╝ ██╗",
                "╚═╝  ╚═╝",
            ]),
        ),
        (
            'y',
            Letter::new([
                "██╗   ██╗",
                "╚██╗ ██╔╝",
                " ╚████╔╝ ",
                "  ╚██╔╝  ",
                "   ██║   ",
                "   ╚═╝   ",
            ]),
        ),
        (
            'z',
            Letter::new([
                "███████╗",
                "╚══███╔╝",
                "  ███╔╝ ",
                " ███╔╝  ",
                "███████╗",
                "╚══════╝",
            ]),
        ),
        (' ', Letter::new(["   ", "   ", "   ", "   ", "   ", "   "])),
    ])
});

fn available_characters() -> String {
    let specials: Vec<&char> = FONT
        .iter()
        .map(|(c, _l)| c)
        .filter(|c| !c.is_ascii_alphanumeric())
        .collect();

    format!("[A-Z, 0-9, {specials:?}")
}

#[derive(Debug, PartialEq)]
struct Letter<'a> {
    repr: [&'a str; 6],
    width: usize,
}

impl<'a> Letter<'a> {
    pub fn new(repr: [&'a str; 6]) -> Self {
        Letter {
            repr,
            width: repr[0].chars().count(),
        } // Using count because █ is 4 bytes
    }

    pub fn draw(&self, rb: &RustBox, start_x: usize, start_y: usize) {
        let mut y = start_y;
        for line in self.repr {
            rb.print(
                start_x,
                y,
                rustbox::RB_NORMAL,
                Color::Default,
                Color::Default,
                line,
            );
            y += 1;
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Text<'a> {
    letters: Vec<&'a Letter<'a>>,
}

impl<'a> Text<'a> {
    pub fn total_width(&self) -> usize {
        self.letters.clone().iter().map(|l| l.width).sum()
    }

    pub fn draw(&self, rb: &RustBox, start_x: usize, start_y: usize) {
        let mut x = start_x;
        for letter in self.letters.iter() {
            letter.draw(rb, x, start_y);
            x += letter.width;
        }
    }
}

impl<'a> FromStr for Text<'a> {
    type Err = FontError;

    fn from_str(s: &str) -> Result<Self> {
        if s.is_empty() {
            return Err(FontError::WrongTextSize);
        }

        let letters = panic::catch_unwind(|| s.to_lowercase().chars().map(|c| &FONT[&c]).collect())
            .map_err(|_| FontError::InvalidCharacter(available_characters()))?;

        Ok(Text { letters })
    }
}

#[derive(Error, Debug)]
pub enum FontError {
    #[error("Wrong font input size")]
    WrongTextSize,

    #[error("Invalid Character, Available characters: {0}")]
    InvalidCharacter(String),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_should_create_the_font() {
        let expected = Letter {
            repr: [
                " █████╗ ",
                "██╔══██╗",
                "███████║",
                "██╔══██║",
                "██║  ██║",
                "╚═╝  ╚═╝",
            ],
            width: 8,
        };

        assert_eq!(FONT[&'a'], expected);
    }

    #[test]
    #[should_panic]
    fn it_should_panic_when_getting_wrong_letter() {
        let _ = &FONT[&'$'];
    }

    #[test]
    fn it_should_create_text_from_str() {
        let res_str: Result<Text> = "aob".parse();
        assert!(res_str.is_ok());
        let res: Text = res_str.unwrap();
        assert_eq!(res.total_width(), 25);
        assert_eq!(res.letters.len(), 3);

        let expected = Letter {
            repr: [
                " █████╗ ",
                "██╔══██╗",
                "███████║",
                "██╔══██║",
                "██║  ██║",
                "╚═╝  ╚═╝",
            ],
            width: 8,
        };
        assert_eq!(res.letters[0], &expected);
    }

    #[test]
    fn it_should_raise_wrong_size() {
        let res_str: Result<Text> = "".parse();
        assert!(res_str.is_err());
        let er = res_str.err().unwrap();
        assert_eq!(er.to_string(), FontError::WrongTextSize.to_string());
    }

    #[test]
    fn it_should_raise_illegal_character() {
        let res_str: Result<Text> = "a&b".parse();
        assert!(res_str.is_err());
        let er = res_str.err().unwrap();
        assert_eq!(
            er.to_string(),
            FontError::InvalidCharacter(available_characters()).to_string()
        );
    }
}
