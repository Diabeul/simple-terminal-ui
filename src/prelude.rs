pub use crate::text::FontError;

pub type Result<T> = std::result::Result<T, FontError>;
