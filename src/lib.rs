mod prelude;
mod text;
mod tui;

use prelude::*;

pub use tui::Tui;

pub fn simple_tui(txt: &str) -> Result<()> {
    Tui::default().render(txt)?;
    Ok(())
}
